import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(items: any[], searchExp: string): any[] {
    if (!items) { return []; }
    if (!searchExp) { return items; }

    searchExp = searchExp.toLowerCase();
    return items.filter(it => {
      if(it.value.Title){
        return it.value.Title.toString().toLowerCase().includes(searchExp); // for key value movie pairs only
      }
    });
  }

}
