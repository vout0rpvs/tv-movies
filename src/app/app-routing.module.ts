import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TvMoviesComponent } from './components/tv-movies/tv-movies.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { SignupComponent } from './components/signup/signup.component';
import { FavoritesComponent } from './components/favorites/favorites.component';

const routes: Routes = [
  { path: 'tv-movies', component: TvMoviesComponent, canActivate: [AuthGuardService], data: { depth: 2} },
  { path: 'login', component: LoginComponent, data: { depth: 1}},
  { path: 'signup', component: SignupComponent, data: { depth: 3}},
  { path: 'favorites', component: FavoritesComponent, data: { depth: 4}},
  { path: '', redirectTo: 'tv-movies', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
