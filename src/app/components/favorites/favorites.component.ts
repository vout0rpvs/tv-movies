import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { TvMoviesService } from '../../services/tv-movies.service';
import { MainService } from '../../services/main.service';
import * as Rx from 'rxjs';
@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
  currUser$: Observable<any>;
  favouriteMovies: any;
  constructor(private tvService: TvMoviesService,
    private mainService: MainService) {
    this.currUser$ = this.mainService.currUser;
    if (localStorage.getItem('currentUser')) {
      Rx.of(JSON.parse(localStorage.getItem('currentUser'))).subscribe(data => {
        this.mainService.currUser.next(data);
      });
    }
  }

  getUserFavs = () => {
    this.currUser$.pipe(switchMap(user => {
      return this.tvService.getFavorites(user.key);
    })).subscribe(favs => {
      this.favouriteMovies = favs;
    });
  }

  ngOnInit() {
    this.getUserFavs();
  }

}
