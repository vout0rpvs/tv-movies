import {
  Component,
  OnInit,
  AfterViewInit
} from '@angular/core';
import {
  MainService
} from '../../services/main.service';
import {
  Observable
} from 'rxjs';
import {
  TvMoviesService
} from '../../services/tv-movies.service';
import * as Rx from 'rxjs';
import {
  switchMap,
  mergeMap
} from 'rxjs/operators';
import {
  ToastrService
} from 'ngx-toastr';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-tv-movies',
  templateUrl: './tv-movies.component.html',
  styleUrls: ['./tv-movies.component.scss']
})
export class TvMoviesComponent implements OnInit, AfterViewInit {
  currFav: any;
  currUser$: Observable < any > ;
  currentUsername: any;

  movies$: Observable < any > ;
  movies = [];
  favouriteMovies: any;

  userSub: Rx.Subscription;
  constructor(private mainService: MainService,
    private tvService: TvMoviesService) {

  }
  getUserFavs = () => {
    this.userSub = this.currUser$.pipe(switchMap(user => {
      return this.tvService.getFavorites(user.key);
    })).subscribe(favs => {
      this.favouriteMovies = favs;
      this.movies$.pipe(mergeMap(x => x)).subscribe(movie => {
        movie['value'].isFavorite = false;
        this.favouriteMovies.map(favMovie => {
          if (favMovie.value.Title === movie['value'].Title) {
            movie['value'].isFavorite = true;
          }
        });
        this.movies.push(movie);
      });
    });
  }
  clearFavs = key => {
    this.tvService.clearFavorites(key);
    this.movies.forEach(movie => {
      movie.value.isFavorite = false;
    });
  }
  addToFavs = (ukey, movie, i) => {
    this.userSub.unsubscribe();
    if (movie.isFavorite) {
      this.favouriteMovies.forEach(fav => {
        if (fav.value.Title === movie.Title) {
          this.tvService.removeFromFavorites(ukey, fav.key);
        }
         console.log(movie.isFavorite);
        movie.isFavorite = false;
      });
    } else {
      this.tvService.addFavMovieToUser(ukey, movie);
      movie.isFavorite = true;
    }
  }
  ngOnInit(): void {
    this.currUser$ = this.mainService.currUser;
    if (localStorage.getItem('currentUser')) {
      Rx.of(JSON.parse(localStorage.getItem('currentUser')))
      .subscribe(data => {
        this.mainService.currUser.next(data);
        this.getUserFavs();
        this.movies$ = this.tvService.getMovies();
      });
    }
  }
  ngAfterViewInit(): void {
    if (localStorage.getItem('currentUser')) {
      Rx.of(JSON.parse(localStorage.getItem('currentUser')))
      .subscribe(data => {
        this.mainService.currUser.next(data);
        this.getUserFavs();
        this.movies$ = this.tvService.getMovies();
      });
    }
  }
}
