import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup
} from '@angular/forms';
import { Router } from '@angular/router';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  errorMessage: String = '';
  constructor(private fb: FormBuilder,
    private router: Router,
    private mainService: MainService) {
    this.createForm();
  }
  createForm = () => {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  private afterSignIn(): void {
    // Do after login stuff here, such router redirects, toast messages, etc.
    this.router.navigate(['/admin']);
  }
  credentialsLogin = value => {
    this.mainService.credentialsLogin(value);
  }
  validation = (event , cred) => {
    if (event.target.value === '') {
    }
  }

  ngOnInit() {

  }

}
