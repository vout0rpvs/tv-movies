import { Component, OnInit } from '@angular/core';
import { MainService } from '../../services/main.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signUpForm: FormGroup;
  constructor(private mainService: MainService,
    private router: Router,
    private fb: FormBuilder) {
      this.signUpForm = this.fb.group({
        name: new FormControl('', Validators.required),
        email: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
      });
    }
  register = (value) => {
    this.mainService.basicSignUp(value).then(() => {
      localStorage.removeItem('currentUser');
    });
  }
  ngOnInit() {
  }

}
