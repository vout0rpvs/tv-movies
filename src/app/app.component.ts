import { Component } from '@angular/core';
import { trigger, transition, group, query, style, animate} from '@angular/animations';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('routeAnimation', [
        transition('1 => 2, 2 => 3, 2 => 4, 3 => 4', [
            style({ height: '!' }),
            query(':enter', style({ transform: 'translateX(100%)' }), { optional: true}),
            query(':enter, :leave', style({ position: 'absolute', top: 0, left: 0, right: 0 }), { optional: true}),
            // animate the leave page away
            group([
                query(':leave', [
                    animate('0.3s cubic-bezier(.35,0,.25,1)', style({ transform: 'translateX(-100%)' })),
                ], { optional: true}),
                // and now reveal the enter
                query(':enter', animate('0.3s cubic-bezier(.35,0,.25,1)', style({ transform: 'translateX(0)' })), { optional: true}),
            ]),
        ]),
        transition('4 => 3, 4 => 2, 3 => 1, 3 => 2, 2 => 1', [
            style({ height: '!' }),
            query(':enter', style({ transform: 'translateX(-100%)' }), { optional: true}),
            query(':enter, :leave', style({ position: 'absolute', top: 0, left: 0, right: 0 }), { optional: true}),
            // animate the leave page away
            group([
                query(':leave', [
                    animate('0.3s cubic-bezier(.35,0,.25,1)', style({ transform: 'translateX(100%)' })),
                ], { optional: true}),
                // and now reveal the enter
                query(':enter', animate('0.3s cubic-bezier(.35,0,.25,1)', style({ transform: 'translateX(0)' })), { optional: true}),
            ]),
        ]),
    ])
]
})
export class AppComponent {
    constructor(public router: Router,
        public activatedRoute: ActivatedRoute) {}
        getDepth = outlet => {
           return outlet.activatedRouteData['depth'];
         }
}
