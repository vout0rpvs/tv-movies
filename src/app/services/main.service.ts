import {
  Injectable
} from '@angular/core';
import {
  AngularFireAuth
} from '@angular/fire/auth';
import {
  Router
} from '@angular/router';
import {
  BehaviorSubject,
  Subject,
  Observable
} from 'rxjs';
import {
  AngularFireDatabase
} from '@angular/fire/database';
import {
  User
} from '../models/user.model';
import {
  config
} from '../models/config.model';
import {
  map
} from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class MainService {
  authState: any = null;
  currUser = new BehaviorSubject<any>(Object);
  user = new Subject < firebase.User > ();
  constructor(private auth: AngularFireAuth,
    private router: Router,
    private db: AngularFireDatabase,
    private toastr: ToastrService) {
    this.auth.authState.subscribe(state => {
      this.authState = state;
    });

  }
  get authenticated(): boolean {
    return this.authState !== null;
  }
  get currentUserObservable(): Observable<any> {
    return this.auth.authState;
  }

  getUsers = () => {
    return this.db.list(`/${config.users}`).snapshotChanges()
    .pipe(map(changes =>
      changes.map(c => ({key: c.payload.key, value: c.payload.val()}))));
  }

  credentialsLogin = ({email, password}) => {
    return this.auth.auth.signInWithEmailAndPassword(email, password)
      .then(user => {
        this.authState = user;
        this.getUsers().subscribe(creds => {
          creds.map(c => {
            if (c.value['email'] === user.user.email) {
              this.currUser.next(c);
              localStorage.setItem('currentUser', JSON.stringify(c));
            }
          });
        });
        this.router.navigate(['tv-movies']);
        this.toastr.success('Thank you for your time! Enjoy :)');
      })
      .catch(err => {
        this.toastr.error('I am terribly sorry but \n' + err.message + ' \n Please try again!');
      });
  }
  signOut = () => {
    this.auth.auth.signOut();
    this.router.navigate(['login']);
  }

  basicSignUp = (credentials) => {
    console.log(credentials);
    return this.auth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password)
    .then(() => {
      this.db.list(`${config.users}`).push(credentials);
      this.router.navigate(['login']);
    })
    .catch(err => this.toastr.error('Something is terribly wrong \n' + err.msg));
  }



}
