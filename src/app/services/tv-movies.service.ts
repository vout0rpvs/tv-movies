import {
  Injectable
} from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { map, take, mergeMap } from 'rxjs/operators';
import { config } from '../models/config.model';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class TvMoviesService {
  movies = [];

  constructor(private db: AngularFireDatabase, private toastr: ToastrService) {}
  addMovies = (movie) => {
      this.db.list(`${config.movies}`).push(movie);
  }
  getMovies = () => {
    return this.db.list(`${config.movies}`)
    .snapshotChanges()
    .pipe(
      map(changes =>
      changes.map(c => ({key: c.payload.key, value: c.payload.val()}))));
  }
  getFavorites = (id) => {
    return this.db.list(`${config.users}/${id}/favorite_movie_or_sitcom`)
    .snapshotChanges()
    .pipe(
      map(changes => changes.map(c => ({key: c.key, value: c.payload.val()}))
    ));
  }
  removeFromFavorites = (uid, mid) => {
    return this.db.list(`${config.users}/${uid}/favorite_movie_or_sitcom/${mid}`).remove();
  }
  clearFavorites = id => {
    return this.db.list(`${config.users}/${id}/favorite_movie_or_sitcom`).remove().then(() => {
      this.toastr.success('All favorites cleared');
    });
  }
  addFavMovieToUser = (id , favorites) => {
    const dbRef = this.db.list(`${config.users}/${id}/favorite_movie_or_sitcom`);
    return dbRef.push(favorites).then(() => {
      this.toastr.success('Added to favorites \n' + favorites.Title);
    });
  }
}
