import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router } from '@angular/router';
import { MainService } from './main.service';

import { Observable } from 'rxjs';
import { map, tap, take } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(
    private router: Router,
    private mainService: MainService,
    private toastr: ToastrService) { }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | boolean {
    if (this.mainService.authenticated) {
      return true;
    }
    return this.mainService.currentUserObservable
    .pipe(
      take(1),
      map(user => !!user),
      tap(loggedIn => {
        if (!loggedIn) {
          this.toastr.error('Access Denied, please log in');
          this.router.navigate(['login']);
        }
      }));
  }
}
